package citamedica;
import java.io.* ; 
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
class ResultToPrintOnFile implements Serializable{
     public static String name ; 
     public static List<Identificador> getIdentificadorsFromFile (){

            File f = new File(name) ;
        try { 
            FileInputStream fis = new FileInputStream(f) ;
            ObjectInputStream ois = new ObjectInputStream(fis) ; 
           List<Identificador> list = (ArrayList<Identificador>)ois.readObject();
           fis.close();
           ois.close();
                return list ;   
        } catch (FileNotFoundException ex) {
         //   ex.printStackTrace();
        } catch (IOException ex) {
           //             ex.printStackTrace();

        } catch (ClassNotFoundException ex) {
            //ex.printStackTrace();
        }
        return null ;
         
    }
     public static List<Identificador> getTempIdentificadorsFromFile (){
    
            File f = new File(name) ;
        try { 
            FileInputStream fis = new FileInputStream(f) ;
            ObjectInputStream ois = new ObjectInputStream(fis) ; 
            ois.readObject();
           List<Identificador> list = (ArrayList<Identificador>)ois.readObject();
           fis.close();
           ois.close();
                return list ;   
        } catch (FileNotFoundException ex) {
         //   ex.printStackTrace();
        } catch (IOException ex) {
           //             ex.printStackTrace();

        } catch (ClassNotFoundException ex) {
            //ex.printStackTrace();
        }
return null  ;         
    }
     public static  boolean printToFile (){
        try {        
            File f = new File(name) ; 
            ObjectOutputStream oos;
            FileOutputStream fos = new FileOutputStream(f) ;
            if (f.length()==0){
                  oos = new ObjectOutputStream(fos) ;}
            else {
                  oos = new myObjectOutputStream(fos) ;}
             oos.writeObject(CitaMedica.identificadors);
             oos.writeObject(CitaMedica.tempIdentificadors);
             oos.close();
             fos.close();
        } catch (FileNotFoundException ex) {
            return false ;         } 
        catch (IOException ex) {
        }
        return true ; 
    }
      public static  boolean printToBackUpFile ( String name ){
        try {        
            File f = new File(name) ; 
            ObjectOutputStream oos;
            FileOutputStream fos = new FileOutputStream(f) ;
            if (f.length()==0){
                  oos = new ObjectOutputStream(fos) ;}
            else {
                  oos = new myObjectOutputStream(fos) ;}
        //    System.out.println(CitaMedica.identificadors);
             oos.writeObject(CitaMedica.identificadors);
             oos.writeObject(CitaMedica.tempIdentificadors);
             oos.close();
             fos.close();
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
            return false ;         } 
        catch (IOException ex) {
               System.out.println(ex.getMessage());
        }
        return true ; 
    }
     public static void printFromFile (){
            File f = new File(name) ;
        try { 
            FileInputStream fis = new FileInputStream(f) ;
            ObjectInputStream ois = new ObjectInputStream(fis) ; 
         
                List<Identificador>identificadors = (ArrayList<Identificador>)ois.readObject();
                List<Identificador>tempIdentificadorsList = (ArrayList<Identificador>)ois.readObject();
                estadisticas(identificadors, tempIdentificadorsList);
                mostrarHistorico(identificadors);
            //    System.out.println(identificadors);
           //   System.out.println(tempIdentificadorsList);
               
                fis.close();
               ois.close();
                
           
        } catch (FileNotFoundException ex) {
         //   ex.printStackTrace();
        } catch (IOException ex) {
           //             ex.printStackTrace();

        } catch (ClassNotFoundException ex) {
            //ex.printStackTrace();
        }
        
                
    }
     public static void mostrarHistorico (List<Identificador>identificadors) {
             System.out.println(" id        | login time                        |review time    |logout time  ");
                for (Identificador i : CitaMedica.identificadors){
                      System.out.println(i.getId() +"¨  |   "+
                              i.getLoginTime() +"        |"+
                              i.getReviewTime() + "     |" +i.getLogoutTime());
             }
         }
     public static void  estadisticas (List<Identificador>identificadors 
            ,List<Identificador> tempIdentificadors) {
             System.out.println("total identificadors :"+identificadors.size());
             int reviwedIdentificadors =(identificadors.size()
             -tempIdentificadors.size());
             System.out.println("reviewed identificadors :"+reviwedIdentificadors);
             int waitingHours =0 ;
             double avgOfWaiting ;
             for (Identificador i : identificadors){
                  if(i.getReviewTime()!=null)
                      waitingHours += i.getDurationSeconds();
              }
              if (reviwedIdentificadors!=0){
                  avgOfWaiting = waitingHours/reviwedIdentificadors ; 
             System.out.println("the avg of waiting :"+avgOfWaiting);
              } 
              else {
                  System.out.println("c");
              }
         }     
}
public class CitaMedica {
    static Scanner Lector;
    static List<Identificador>identificadors , tempIdentificadors  ;
    	//menu
	 public static void printList() {
		            System.out.println("******************************************************|");
		 	    System.out.println("            ------BIENVENIDO !!-----                  |");
		 	    System.out.println("******************************************************|");
		 	    System.out.println("-Elige una opcion:                                    |");
                            System.out.println("1) Generar un nuevo identificador.");
                            System.out.println("2) Llamar al siguiente paciente.");
                            System.out.println("3) Llamar a un determinado paciente (seleccionando el identificador)");
                            System.out.println("4) Mostrar la lista de espera actual.");
                            System.out.println("5) Mostrar pantalla de llamadas.");
                            System.out.println("6) Marcar paciente como ya atendido (finalizada consulta).");
                            System.out.println("7) Mostrar histórico diario.");
                            System.out.println("8) Estadísticas del dia.");                            
         }
         
         public static void nuevoIdentificador() {
             Identificador temp =new Identificador();
             identificadors.add(temp) ;
             tempIdentificadors.add(temp) ;
             System.out.println("done !! the id is : "+temp.getId());
         }
         
         public static String llamarSiguiente() {
             if(!tempIdentificadors.isEmpty()){
              Identificador temp = tempIdentificadors.remove(0);
              for (int i = 0 ;i<identificadors.size();i++){
                  if(identificadors.get(i).getId().equals(temp.getId())){
                      identificadors.get(i).setReviewTime(LocalDateTime.now());
                      temp= identificadors.get(i);
                  }
              }
            return temp.toString() ;
         }
         return("no identificdors there ");
}
         
         public static void llamarPaciente (String id ) {
             for ( Identificador i :identificadors){
                 if(i.getId().equals(id)){
                     System.out.println("identificador with this id " +id 
                             +"ïs here and these are his/her informations :\n"
                             +i.toString());
                     return ;
                 }
             }
             System.out.println("there is no identificador with this id "+ id );
         }
         
         public static void mostrarLista () {
             System.out.println(" id "+ "¨  |   "+ "¨login time ");
             for (Identificador i : tempIdentificadors){
                 System.out.println(i.getId() +"¨  |   "+ i.getLoginTime());
             }
         }
         
         public static void mostrarPantalla () {
             int temp = identificadors.size()-tempIdentificadors.size() ;
             if (temp == 0){
                 System.out.println("you didnt get any ");
             }
             else
             if(temp <=5){
                 System.out.println("temp " +temp );
                 for ( int i =0 ;i <temp ;i++){
                     System.out.println(identificadors.get(i).getId());
             }
             }
             else{
                  System.out.println("temp2 " +temp );

                temp = temp -5 ;//5
                int t= temp+5 ;//6
                 for (int i = temp ;i < t ;i++){
                      System.out.println(identificadors.get(i).getId());
             }
                 
             }
         }
         
         public static void pacienteAtendido (String id) {
            for ( Identificador i :identificadors){
                 if(i.getId().equals(id)){
                    i.printDuration();
                     return ;
                 }
             }
             System.out.println("there is no identificador with this id "+ id );
     
         }
            
         public static List<Identificador> mostrarHistorico () {
           
             System.out.println(" id        | login time                        |review time    |logout time  ");
                for (Identificador i : identificadors){
                  
                 System.out.println(i.getId() +"¨  |   "+ i.getLoginTime() +"        |"+
                         i.getReviewTime() + "     |" +i.getLogoutTime());
             }
                return identificadors ; 
         
         }
         
         public static void  estadisticas () {
 
             System.out.println("total identificadors :"+identificadors.size());
             int reviwedIdentificadors =(identificadors.size()
             -tempIdentificadors.size());
             System.out.println("reviewed identificadors :"+reviwedIdentificadors);
             int waitingHours =0 ;
             double avgOfWaiting ;
              for (Identificador i : identificadors){
                  if(i.getReviewTime()!=null)
                  waitingHours += i.getDurationSeconds();
              }
              if (reviwedIdentificadors!=0){
                  avgOfWaiting = waitingHours/reviwedIdentificadors ; 
             System.out.println("the avg of waiting :"+avgOfWaiting);
             
              }  else {
                  System.out.println("c");
              }
              
           
         }
         public static void saveData(){
             ResultToPrintOnFile.printToFile() ;
         }
         public static void init (){
            Lector = new Scanner(System.in);
            ResultToPrintOnFile.name = Lector.next() ;
            tempIdentificadors = ResultToPrintOnFile.getTempIdentificadorsFromFile() ;
            identificadors = ResultToPrintOnFile.getIdentificadorsFromFile() ;
            BackUpThread but = new BackUpThread();
            but.start();
   }
         public static void main(String[] args) {
                init();
	        boolean salir = false ;
	        int opcion;
	        while (!salir) {
	    		printList();
	    		try {
	    			opcion = Lector.nextInt();
	        	
	    		switch (opcion) {
	    		case 1:
                                System.out.println("el identificador de este paciente es: ");
	    			nuevoIdentificador();
	    			break;
	    			
	    		case 2: 
	    			 System.out.println(llamarSiguiente());
	    			break;
	    			
	    		case 3: 
                            
	    			llamarPaciente (Lector.next());
	    			break;
	    			
	    		case 4:
	    			mostrarLista ();
	    			break;
	    			
	    		case 5: 
	    			mostrarPantalla (); 
	    			break;
	    			
	    		case 6:
	    			pacienteAtendido (Lector.next());
	    			break;
	    			
	    		case 7:
                                mostrarHistorico ();
	                        break;
	    			
	    		case 8: 
	    			estadisticas ();
	    			break;
                        case 9 :
                            saveData() ; 
                            break ; 
	    		case 11:
	    			salir=true;
                                ResultToPrintOnFile
                                        .printToFile();
	    			System.out.println("Adioooooos!!!!!");
	    			break;
	    			
	    		}
	        } catch (InputMismatchException e) {
	            System.out.println("Debes insertar un numero");
	            Lector.next();
		}
	    		
	        }

	}
	

    }
    

