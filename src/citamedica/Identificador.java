
package citamedica;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.HashMap;
public class Identificador implements Serializable{
    private LocalDateTime loginTime , logoutTime , reviewTime ;
    private String id ;    
    public void printDuration (){
        logoutTime = LocalDateTime.now();
        Period period = getPeriod(loginTime, reviewTime);
        long time[] = getTime(loginTime, reviewTime);
        System.out.println(period.getYears() + " years " + 
                period.getMonths() + " months " + 
                period.getDays() + " days " +
                time[0] + " hours " +
                time[1] + " minutes " +
                time[2] + " seconds.");
    }
    public int getDurationSeconds (){
         long time[] = getTime(loginTime, reviewTime);
         return (int)time[2];
    }
    public Identificador(){
      ids= new HashMap<>();
        id = genrateRandomUniqeId();
        loginTime =LocalDateTime.now();
    }
    private static HashMap<String , String > ids ;
    public String genrateRandomUniqeId(){
        String temp =""+ rndChar()+rndNumber()+rndChar()+rndNumber();
        while (ids.containsKey(temp)){
            temp =""+ rndChar()+rndNumber()+rndChar()+rndNumber();
        }
        ids.put(temp, temp) ;
        return temp ;
    }
    private char rndChar () {
    int rnd = (int) (Math.random() * 52); // or use Random or whatever
    char base = (rnd < 26) ? 'A' : 'a';
    return (char) (base + rnd % 26);
}
    private int rndNumber(){
        return  (int)(Math.random()*9) ;
    }

    public LocalDateTime getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(LocalDateTime loginTime) {
        this.loginTime = loginTime;
    }

    public LocalDateTime getLogoutTime() {
        return logoutTime;
    }

    public void setLogoutTime(LocalDateTime logoutTime) {
        this.logoutTime = logoutTime;
    }

    public LocalDateTime getReviewTime() {
        return reviewTime;
    }

    public void setReviewTime(LocalDateTime reviewTime) {
        this.reviewTime = reviewTime;
    }

  

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static HashMap<String, String> getIds() {
        return ids;
    }

    public static void setIds(HashMap<String, String> ids) {
        Identificador.ids = ids;
    }
    private  Period getPeriod(LocalDateTime dob, LocalDateTime now) {
        return Period.between(dob.toLocalDate(), now.toLocalDate());
    }

    private  long[] getTime(LocalDateTime dob, LocalDateTime now) {
        LocalDateTime today = LocalDateTime.of(now.getYear(),
                now.getMonthValue(), now.getDayOfMonth(), dob.getHour(), dob.getMinute(), dob.getSecond());
        Duration duration = Duration.between(today, now);

        long seconds = duration.getSeconds();

        long hours = seconds / 3600;
        long minutes = ((seconds % 3600) / 60);
        long secs = (seconds % 60);

        return new long[]{hours, minutes, secs};
    }


    @Override
    public String toString() {
        return "Identificador{" + "loginTime=" + loginTime 
                + ", logoutTime=" +( (logoutTime==null) ?"¨not yet " :logoutTime )
                + ", reviewTime=" + reviewTime + ", id=" + id + '}';
    }
    
}
